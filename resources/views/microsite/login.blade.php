@extends('layout.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    {{-- start item --}}
                    <div class="text-center pb-2">
                        <img src="{{ asset('img/logo.png') }}" class="mt-2" alt="">
            
                        <p class="p-greeting">selamat datang</p>
                        <div class="ps-3 pe-3">
                            <p class="p-description-1 pb-4" style="border-bottom: 1px solid #C2C2C2;">
                                The Burger Lovers adalah restoran fast food terunik di Indonesia yang diawali pada tahun 2015 di Jakarta. Dengan produk unggulan berupa Burger dan Shakes, The Burger Lovers hingga saat ini telah memiliki 10 restoran yang tersebar di kota-kota besar di Indonesia.
            
                                Untuk melakukan pemesanan, silahkan isi data diri Anda pada formulir di bawah.
                            </p>
                        </div>
                        <div class="text-start pe-3 ps-3">
                            <p class="p-help-login">masukan data kamu</p>
                        </div>
                        <div class="form-group text-start pe-3 ps-3">
                            <label for="">Nama</label>
                            <input type="text" class="form-control" value="{{$nama}}">
                        </div>
                        <div class="form-group text-start pe-3 ps-3 mt-3">
                            <label for="">Nomor Whatsapp</label>
                            <input type="text" class="form-control" value="{{$no}}">
                        </div>
                        <div class="form-group text-start pe-3 ps-3 mt-3 justify-content-center d-flex">
                            <div class="d-flex align-items-center">
                                <img src="{{ asset('img/ellipse.png') }}" class="icon-choose-takeaway selected-type" is-active="0" style="height: 16px; width: auto;" alt="...">
                                <span class="ms-2" style="cursor: pointer;" onclick="chooseType('takeaway')">Take Away</span>
                            </div>
                            <div class="d-flex align-items-center ms-4">
                                <img src="{{ asset('img/ellipse.png') }}" class="icon-choose-delivery selected-type" is-active="0" style="height: 16px; width: auto;" alt="...">
                                <span class="ms-2" style="cursor: pointer;" onclick="chooseType('delivery')">Delivery</span>
                            </div>
                        </div>
                        <span class="error-type" style="display: none;"></span>
                        <div class="form-group text-start pe-3 ps-3 mt-3">
                            <button class="btn btn-order" onclick="firstOrder()">Saya mau pesan</button>
                        </div>
                    </div>
                    {{-- end item --}}
                </div>
                <div class="carousel-item">
                    {{-- start item --}}
                    <div class="text-center pb-2" style="margin-bottom: 6em;">
                        <img src="{{ asset('img/logo.png') }}" class="mt-2" alt="">

                        <div class="parent-menu">
                            <div class="mt-5 text-start pe-3 ps-3">
                                <img src="{{ asset('img/Burgers.svg') }}" alt="">
                            </div>
    
                            {{-- menu --}}
                            <div>
                                <div class="mt-3 row text-start ps-3 pe-3">
                                    <div class="col-12 pe-0 col-menu">
                                        <div>
                                            <img src="{{ asset('img/burger1.svg') }}" alt="">
                                        </div>
                                        <div style="margin-right: 4em;">
                                            <p class="menu-title text-wrap">Classic dry-aged beef</p>
                                            <div style="width: 9em;">
                                                <p class="helper-menu text-wrap">Patty, Barbecue, Onion Ring, Bacon, Cheddar Cheese</p>
                                            </div>
                                        </div>
                                        <div class="text-end me-1">
                                            <p class="price-menu" id="price-menu-1">30000</p>
                                        </div>
                                    </div>
                                </div>
                                {{-- comment --}}
                                <div class="row mt-2 text-start ps-3 pe-3">
                                    <div class="col-8 me-5">
                                        <div class="group-note">
                                            <input type="text" class="note-menu" id="note-menu-1" data-key="0">
                                            <span class="img-note" data-id="1">
                                                <img src="{{ asset('img/noteinactive.png') }}" id="icon-note-1" alt="">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-2 d-flex align-items-center justify-content-end">
                                        <img src="{{ asset('img/minus.png') }}" class="decrease-qty" onclick="updateQty('1', 'decrease')" alt="">
                                        <span class="me-2 ms-2 qty-menu" id="qty-menu-1">0</span>
                                        <img src="{{ asset('img/plus.png') }}" class="increase-qty" onclick="updateQty('1', 'increase')" alt="">
                                    </div>
                                </div>
                                <div class="pe-3 ps-3">
                                    <hr>
                                </div>
                            </div>
                            <div class="mt-3">
                                <div class="row text-start ps-3 pe-3">
                                    <div class="col-12 pe-0 col-menu">
                                        <div>
                                            <img src="{{ asset('img/burger2.svg') }}" alt="">
                                        </div>
                                        <div style="margin-right: 4em;">
                                            <p class="menu-title text-wrap">Classic Grilled Chicken</p>
                                            <div style="width: 9em;">
                                                <p class="helper-menu text-wrap">Chicken, Chipotle Aioli, Mixed Greens, Roma Tomato, Avocado</p>
                                            </div>
                                        </div>
                                        <div class="text-end me-1">
                                            <p class="price-menu" id="price-menu-2">75000</p>
                                        </div>
                                    </div>
                                </div>
                                {{-- comment --}}
                                <div class="row mt-2 text-start ps-3 pe-3">
                                    <div class="col-8 me-5">
                                        <div class="group-note">
                                            <input type="text" class="note-menu" id="note-menu-2" data-key="0">
                                            <span class="img-note" data-id="2">
                                                <img src="{{ asset('img/noteinactive.png') }}" id="icon-note-2" alt="">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-2 d-flex align-items-center justify-content-end">
                                        <img src="{{ asset('img/minus.png') }}" class="decrease-qty" onclick="updateQty('2', 'decrease')" alt="">
                                        <span class="me-2 ms-2 qty-menu" id="qty-menu-2">0</span>
                                        <img src="{{ asset('img/plus.png') }}" class="increase-qty" onclick="updateQty('2', 'increase')" alt="">
                                    </div>
                                </div>
                                <div class="pe-3 ps-3">
                                    <hr>
                                </div>
                            </div>
                            <div class="mt-3">
                                <div class="row text-start ps-3 pe-3">
                                    <div class="col-12 pe-0 col-menu">
                                        <div>
                                            <img src="{{ asset('img/burger3.svg') }}" alt="">
                                        </div>
                                        <div style="margin-right: 4em;">
                                            <p class="menu-title text-wrap">Classic Smoke Spanish Beef</p>
                                            <div style="width: 9em;">
                                                <p class="helper-menu text-wrap">Patty, Habanero Aioli, Grilled Jalapeño, Leaf Lettuce, Pepper Jack Cheese</p>
                                            </div>
                                        </div>
                                        <div class="text-end me-1">
                                            <p class="price-menu" id="price-menu-3">55000</p>
                                        </div>
                                    </div>
                                </div>
                                {{-- comment --}}
                                <div class="row mt-2 text-start ps-3 pe-3">
                                    <div class="col-8 me-5">
                                        <div class="group-note">
                                            <input type="text" class="note-menu" id="note-menu-3" data-key="0">
                                            <span class="img-note" data-id="3">
                                                <img src="{{ asset('img/noteinactive.png') }}" id="icon-note-3" alt="">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-2 d-flex align-items-center justify-content-end">
                                        <img src="{{ asset('img/minus.png') }}" class="decrease-qty" onclick="updateQty('3', 'decrease')" alt="">
                                        <span class="me-2 ms-2 qty-menu" id="qty-menu-3">0</span>
                                        <img src="{{ asset('img/plus.png') }}" class="increase-qty" onclick="updateQty('3', 'increase')" alt="">
                                    </div>
                                </div>
                                <div class="pe-3 ps-3">
                                    <hr>
                                </div>
                            </div>
                        </div>

                        <div class="parent-menu">
                            <div class="mt-5 text-start pe-3 ps-3">
                                <img src="{{ asset('img/Salads.svg') }}" alt="">
                            </div>
    
                            {{-- menu --}}
                            <div>
                                <div class="mt-3 row text-start ps-3 pe-3">
                                    <div class="col-12 pe-0 col-menu">
                                        <div>
                                            <img src="{{ asset('img/salad1.svg') }}" alt="">
                                        </div>
                                        <div style="margin-right: 4em;">
                                            <p class="menu-title text-wrap">BBQ Ranch</p>
                                            <div style="width: 9em;">
                                                <p class="helper-menu text-wrap">Fresh greens, sharp cheddar, applewood smoked bacon, tomatoes & haystack onions with ranch dressing & BBQ sauce</p>
                                            </div>
                                        </div>
                                        <div class="text-end me-1">
                                            <p class="price-menu" id="price-menu-4">60000</p>
                                        </div>
                                    </div>
                                </div>
                                {{-- comment --}}
                                <div class="row mt-2 text-start ps-3 pe-3">
                                    <div class="col-8 me-5">
                                        <div class="group-note">
                                            <input type="text" class="note-menu" id="note-menu-4" data-key="0">
                                            <span class="img-note" data-id="4">
                                                <img src="{{ asset('img/noteinactive.png') }}" id="icon-note-4" alt="">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-2 d-flex align-items-center justify-content-end">
                                        <img src="{{ asset('img/minus.png') }}" class="decrease-qty" onclick="updateQty('4', 'decrease')" alt="">
                                        <span class="me-2 ms-2 qty-menu" id="qty-menu-4">0</span>
                                        <img src="{{ asset('img/plus.png') }}" class="increase-qty" onclick="updateQty('4', 'increase')" alt="">
                                    </div>
                                </div>
                                <div class="pe-3 ps-3">
                                    <hr>
                                </div>
                            </div>
                            <div class="mt-3">
                                <div class="row text-start ps-3 pe-3">
                                    <div class="col-12 pe-0 col-menu">
                                        <div>
                                            <img src="{{ asset('img/salad2.svg') }}" alt="">
                                        </div>
                                        <div style="margin-right: 4em;">
                                            <p class="menu-title text-wrap">Classic Cobb</p>
                                            <div style="width: 9em;">
                                                <p class="helper-menu text-wrap">Fresh greens, blue cheese, sharp cheddar, fried egg, applewood smoked bacon, tomatoes & onions with ranch dressing.</p>
                                            </div>
                                        </div>
                                        <div class="text-end me-1">
                                            <p class="price-menu" id="price-menu-5">60000</p>
                                        </div>
                                    </div>
                                </div>
                                {{-- comment --}}
                                <div class="row mt-2 text-start ps-3 pe-3">
                                    <div class="col-8 me-5">
                                        <div class="group-note">
                                            <input type="text" class="note-menu" id="note-menu-5" data-key="0">
                                            <span class="img-note" data-id="5">
                                                <img src="{{ asset('img/noteinactive.png') }}" id="icon-note-5" alt="">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-2 d-flex align-items-center justify-content-end">
                                        <img src="{{ asset('img/minus.png') }}" class="decrease-qty" onclick="updateQty('5', 'decrease')" alt="">
                                        <span class="me-2 ms-2 qty-menu" id="qty-menu-5">0</span>
                                        <img src="{{ asset('img/plus.png') }}" class="increase-qty" onclick="updateQty('5', 'increase')" alt="">
                                    </div>
                                </div>
                                <div class="pe-3 ps-3">
                                    <hr>
                                </div>
                            </div>
                        </div>

                    </div>
                    {{-- end item --}}
                </div>
            </div>     
        </div>
    </div>

</div>
{{-- total badge --}}
<div class="row row-total">
    <div class="col-12 pe-3 ps-3">
        <button class="total btn">
            <div>
                <p class="total-text">
                    <span class="total-item">0</span>
                    <span class="text-item">item</span>
                </p>
            </div>
            <div>
                <p class="total-price">
                    <span class="target-total-price">0</span>
                    <span class="icon-shopping-bag">
                        <img src="{{ asset('img/shoppingbag.svg') }}" alt="">
                    </span>
                </p>
            </div>
        </button>
    </div>
</div>

<script>

    $(document).ready(function() {
        // $('#carouselExampleSlidesOnly').carousel('next');
        $('#carouselExampleSlidesOnly').carousel('pause');
    
        $('.img-note').click(function(e) {
            e.preventDefault();
            var activeNote = "{{ asset('img/note.png') }}";
            var inactiveNote = "{{ asset('img/noteinactive.png') }}";
            var id = $(this).attr('data-id');
            var check = $('#note-menu-' + id).attr('data-key');
            console.log(check);

            if (check == 0) {
                $('#note-menu-' + id).addClass('note-active');
                $('#icon-note-' + id).attr('src', activeNote);
                $('#note-menu-' + id).attr('data-key', '1');
            } else {
                $('#note-menu-' + id).val('');
                $('#note-menu-' + id).removeClass('note-active');
                $('#icon-note-' + id).attr('src', inactiveNote);
                $('#note-menu-' + id).attr('data-key', '0');
            }

        })

        customQty();
    });

    function customQty() {
        var qty = $('.qty-menu');
        if (qty.length == 0) {
            if (document.getElementById('qty-menu-1').innerHTML == 0) {
                document.getElementById('qty-menu-1').classList.add('null-qty');
            } else {
                document.getElementById('qty-menu-1').classList.remove('null-qty');
            }
        } else {
            for (var i = 0; i < qty.length; i++) {
                var check = document.getElementById('qty-menu-' + (i + 1));
                if (check.innerHTML == 0) {
                    document.getElementById('qty-menu-' + (i + 1)).classList.add('null-qty');
                } else {
                    document.getElementById('qty-menu-' + (i + 1)).classList.remove('null-qty');
                }
            }
        }
    }

    function updateQty(id, indicator) {
        var val, tots;
        var currValue = $('#qty-menu-' + id).html();
        var price = $('#price-menu-' + id).html();
        var currTotalQty = $('.total-item').html();

        if (indicator == 'decrease') {
            if (currValue > 0) {
                val = (parseInt(currValue) - 1);
            }

            if (currTotalQty > 0) {
                tots = (parseInt(currTotalQty) - 1);
            }
        } else {
            val = (parseInt(currValue) + 1);
            tots = (parseInt(currTotalQty) + 1);
        }

        $('#qty-menu-' + id).html(val);
        $('.total-item').html(tots);

        customQty();
        updatePrice(price, indicator);
    }

    function updatePrice(price, indicator) {
        var currPrice = $('.target-total-price').html();
        var update;

        if (indicator == 'decrease') {
            if (currPrice > 0) {
                update = (parseInt(currPrice) - parseInt(price));
            }
        } else {
            update = (parseInt(currPrice) + parseInt(price));
        }
        $('.target-total-price').html(update);
    }

    function chooseType(param) {
        var select = "{{ asset('img/checklist.png') }}";
        var unselect = "{{ asset('img/ellipse.png') }}";

        var allSelect = $("img[is-active='1']");

        var check = $(".icon-choose-" + param).attr('is-active');

        if (check == 0) {
            if (allSelect.length > 0) {
                $('.error-type').show();
                $('.error-type').text('You cannot choose all type, select one of them');
            } else {
                $(".icon-choose-" + param).attr('src', select);
                $(".icon-choose-" + param).attr('is-active', '1');
                $('.error-type').hide();
            }
        } else {
            $(".icon-choose-" + param).attr('src', unselect);
            $(".icon-choose-" + param).attr('is-active', '0');
            $('.error-type').hide();
        }
        
    } 
    
    function firstOrder() {
        var allSelect = $("img[is-active='1']");
        
        if (allSelect.length > 0) {
            $('.error-type').hide();
            $('#carouselExampleSlidesOnly').carousel('next');
            $('#carouselExampleSlidesOnly').carousel('pause');
            setTimeout(() => {
                $('.row-total').show();
            }, 1000);
        } else {
            $('.error-type').show();
            $('.error-type').text('Please choose type of order');
            $('.row-total').hide();
        }
    }
</script>
@endsection