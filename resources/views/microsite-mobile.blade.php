@extends('layout.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="text-center pb-2">
            <img src="{{ asset('img/logo.png') }}" class="mt-2" alt="">

            <p class="p-greeting">selamat datang</p>
            <div class="ps-3 pe-3">
                <p class="p-description-1 pb-4" style="border-bottom: 1px solid #C2C2C2;">
                    The Burger Lovers adalah restoran fast food terunik di Indonesia yang diawali pada tahun 2015 di Jakarta. Dengan produk unggulan berupa Burger dan Shakes, The Burger Lovers hingga saat ini telah memiliki 10 restoran yang tersebar di kota-kota besar di Indonesia.

                    Untuk melakukan pemesanan, silahkan isi data diri Anda pada formulir di bawah.
                </p>
            </div>
            <div class="text-start pe-3 ps-3">
                <p class="p-help-login">masukan data anda</p>
            </div>
            <div class="form-group text-start pe-3 ps-3">
                <label for="">Nama</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group text-start pe-3 ps-3 mt-3">
                <label for="">Nomor Whatsapp</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group text-start pe-3 ps-3 mt-3 justify-content-center d-flex">
                <div class="d-flex align-items-center">
                    <img src="{{ asset('img/ellipse.png') }}" class="icon-choose-takeaway selected-type" is-active="0" style="height: 16px; width: auto;" alt="...">
                    <span class="ms-2" style="cursor: pointer;" onclick="chooseType('takeaway')">Take Away</span>
                </div>
                <div class="d-flex align-items-center ms-4">
                    <img src="{{ asset('img/ellipse.png') }}" class="icon-choose-delivery selected-type" is-active="0" style="height: 16px; width: auto;" alt="...">
                    <span class="ms-2" style="cursor: pointer;" onclick="chooseType('delivery')">Delivery</span>
                </div>
            </div>
            <span class="error-type" style="display: none;"></span>
            <div class="form-group text-start pe-3 ps-3 mt-3">
                <button class="btn btn-order" onclick="firstOrder()">Saya mau pesan</button>
            </div>
        </div>
    </div>
</div>

<script>
    function chooseType(param) {
        var select = "{{ asset('img/checklist.png') }}";
        var unselect = "{{ asset('img/ellipse.png') }}";

        var allSelect = $("img[is-active='1']");

        var check = $(".icon-choose-" + param).attr('is-active');

        if (check == 0) {
            if (allSelect.length > 0) {
                $('.error-type').show();
                $('.error-type').text('You cannot choose all type, select one of them');
            } else {
                $(".icon-choose-" + param).attr('src', select);
                $(".icon-choose-" + param).attr('is-active', '1');
                $('.error-type').hide();
            }
        } else {
            $(".icon-choose-" + param).attr('src', unselect);
            $(".icon-choose-" + param).attr('is-active', '0');
            $('.error-type').hide();
        }
        
    } 
    
    function firstOrder() {
        var allSelect = $("img[is-active='1']");
        
        if (allSelect.length > 0) {
            $('.error-type').hide();
        } else {
            $('.error-type').show();
            $('.error-type').text('Please choose type of order');
        }
    }
</script>
@endsection