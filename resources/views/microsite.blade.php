@extends('layout.app')

@section('content')
<div class="row main-row">
    <div class="col-4 col-blur"></div>
    <div class="col-4" style="background: #E5E5E5;">
        <div class="card" style="background: #E5E5E5; border: none;">
            <div class="card-body">
                <div class="text-center">
                    <img src="{{ asset('img/logo.png') }}" alt="">

                    <p class="p-greeting">selamat datang</p>

                    <div>
                        <p class="p-description-1">
                            The Burger Lovers adalah restoran fast food terunik di Indonesia yang diawali pada tahun 2015 di Jakarta. Dengan produk unggulan berupa Burger dan Shakes, The Burger Lovers hingga saat ini telah memiliki 10 restoran yang tersebar di kota-kota besar di Indonesia.

Untuk melakukan pemesanan, silahkan isi data diri Anda pada formulir di bawah.

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4"></div>
</div>
@endsection