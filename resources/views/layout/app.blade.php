<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    {{-- css bootstrap --}}
    <link rel="stylesheet" href="{{ secure_url('bootstrap/css/bootstrap.min.css') }}">
    
    {{-- js bootstrap --}}
    <script src="{{ secure_url('bootstrap/js/bootstrap.min.js') }}"></script>
    
    {{-- jquery --}}
    <script src="{{ secure_url('js/jquery.js') }}"></script>

    <style>
        body {
            height: 100vh;
            overflow: hidden;
            overflow-y: scroll;
            background: #E5E5E5;
        }

        .main-row {
            background: #CCCCCC;
            /* Keep the inherited background full size. */
            background-attachment: fixed; 
            background-size: cover;
        }

        .col-blur {
            height: 100vh;
        }

        .p-greeting {
            color: #932D00;
            text-transform: capitalize;
            font-size: 18px;
            line-height: 22px;
            margin-top: 2em;
            font-weight: normal;
        }

        .p-description-1 {
            font-style: normal;
            font-size: 14px;
            line-height: 24px;
            text-align: left;
            /* or 171% */
        }

        .p-help-login {
            font-style: normal;
            font-weight: bold;
            font-size: 14px;
            line-height: 24px;
            /* identical to box height, or 171% */


            color: #932D00;
            text-transform: capitalize;
        }

        label {
            color: #686868;
            font-style: normal;
            font-weight: normal;
            line-height: 13px;
            margin-bottom: .5em;
        }

        .form-control {
            background: #FFFFFF;
            border: 1px solid #ECECEC;
            box-sizing: border-box;
            border-radius: 8px;
            padding: .7em;
            color: #8D8F92;
            font-style: normal;
            font-weight: 500;
        }

        .form-control:focus {
            outline: none !important;
            box-shadow: none !important;
            border: 1px solid #ECECEC;
        }

        .btn-order {
            width: 100%;
            background: #F6640F;
            border-radius: 8px;
            color: #fff;
            padding-top: 1em;
            padding-bottom: 1em;
            margin-top: 1em;
        }

        .error-type {
            color: red;
            font-size: .8em;
        }

        .menu-title {
            color: #000000;
            text-transform: capitalize;
            font-weight: 600;
            font-size: .9em;
            margin-bottom: .1em;
        }

        .helper-menu {
            color: #525252;
            font-size: .6em;
            margin-bottom: 0;
        }

        .price-menu {
            color: #000000;
            font-weight: 600;
            margin-bottom: 2rem;
        }

        .col-menu {
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .note-menu {
            background: #FFFFFF;
            border-radius: 6px;
            box-shadow: none;
            border: none;
            transition: all .3s;
            width: 0%;
            line-height: 2;
            padding-left: 0;
            padding-right: 0;
        }

        .note-menu:focus {
            border: none;
            outline: none;
            box-shadow: none;
        }

        .note-active {
            width: 100% !important;
            padding-left: 2.5em;
            padding-right: 1.5em;
            z-index: 1;
        }

        .img-note {
            position: absolute;
            left: 1.7em;
            cursor: pointer;
            z-index: 1000;
        }

        .increase-qty,
        .decrease-qty {
            cursor: pointer;
        }

        .qty-menu {
            color: #000000;
            font-weight: 600;
        }

        .null-qty {
            color: #C2C2C2;
        }

        .total {
            display: flex;
            justify-content: space-between;
            align-items: center;
            background: #F6640F;
            border-radius: 6px;
            padding: 1em 1.5em;
            width: 100%;
        }

        .total-text,
        .total-price {
            color: #fff;
            font-weight: bold;
            margin-bottom: 0;
        }

        .row-total {
            position: fixed;
            width: -webkit-fill-available;
            bottom: 4em;
            display: none;
            z-index: 1001;
        }

        hr {
            background: #ADACAC;
        }
    </style>
</head>
<body>
    @yield('content')
</body>
</html>